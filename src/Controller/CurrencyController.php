<?php

namespace App\Controller;

use App\Entity\Currency;
use App\Entity\Rate;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class CurrencyController extends AbstractController
{
    /**
     * @Route("/currencies", name="currencies")
     */
    public function currencies()
    {
        $currencyRepository = $this->getDoctrine()->getRepository(Currency::class);
        $currencies = $currencyRepository->findAll();
        $currencyList = [];
        /** @var Currency $currency */
        foreach ($currencies as $currency) {
            $currencyList[] = [
                'code' => $currency->getCode(),
                'name' => $currency->getName()
            ];
        }

        return new JsonResponse(['data' => $currencyList]);
    }

    /**
     * @Route("/rate/{code}")
     */
    public function rate(string $code)
    {
        $rateRepository = $this->getDoctrine()->getRepository(Rate::class);
        $rate = $rateRepository->findOneBy(['currency' => $code], ['date' => 'DESC']);
        if (null === $rate) {
            return new JsonResponse(null, JsonResponse::HTTP_NOT_FOUND);
        }

        return new JsonResponse(['data' => ['rate' => $rate->getRate()]]);
    }

    /**
     * @Route("/average-rate/{code}")
     */
    public function averageRate(string $code)
    {
        $rateRepository = $this->getDoctrine()->getRepository(Rate::class);
        $rate = $rateRepository->findOneBy(['currency' => $code], ['date' => 'DESC']);
        if (null === $rate) {
            return new JsonResponse(null, JsonResponse::HTTP_NOT_FOUND);
        }

        return new JsonResponse(['data' => ['average' => $rate->getAverage()]]);
    }
}