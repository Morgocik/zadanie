<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RateRepository")
 */
class Rate
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="code")
     */
    private $currency;

    /**
     * @ORM\Column(type="decimal", scale=7)
     */
    private $rate;

    /**
     * @ORM\Column(type="decimal", scale=7)
     */
    private $average;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    public function __construct(Currency $currency, float $rate, float $average, \DateTime $date)
    {
        $this->currency = $currency;
        $this->rate = $rate;
        $this->average = $average;
        $this->date = $date;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    public function getRate(): float
    {
        return $this->rate;
    }

    public function getAverage(): float
    {
        return $this->average;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }
}
