<?php

namespace App\Service;

use App\Entity\Currency;
use App\Entity\Rate;
use App\Repository\RateRepository;
use Doctrine\ORM\EntityManagerInterface;

class RateImporter
{
    const RATES_URL = 'http://api.nbp.pl/api/exchangerates/tables/A';

    private $entityManager;
    private $currenciesRepository;
    private $rateRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->currenciesRepository = $this->entityManager->getRepository(Currency::class);
        $this->rateRepository = $this->entityManager->getRepository(Rate::class);
    }

    public function import()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::RATES_URL);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);

        $json = json_decode($response, true);

        $date = new \DateTime($json[0]['effectiveDate']);

        $lastImport = $this->rateRepository->findOneBy([], ['date' => 'DESC']);

        if (null === $lastImport || $lastImport->getDate()->format('Ymd') != $date->format('Ymd')) {
            foreach ($json[0]['rates'] ?? [] as $rate) {
                $averageRate = $this->calculateAverageRate($rate['code'], $rate['mid']);
                $currency = $this->currenciesRepository->find($rate['code']);
                $this->entityManager->persist(new Rate($currency, $rate['mid'], $averageRate, $date));
            }
            $this->entityManager->flush();
        }
    }

    private function calculateAverageRate(string $code, float $currentRate): float
    {
        $averageRate = $this->rateRepository->calculateAverage($code) ?: $currentRate;

        return ($averageRate + $currentRate) / 2;
    }
}
