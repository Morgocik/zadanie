<?php

namespace App\Service;

use App\Entity\Currency;
use Doctrine\ORM\EntityManagerInterface;

class CurrencyImporter
{
    const CURRENCIES_URL = 'http://api.nbp.pl/api/exchangerates/tables/A';

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function import()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::CURRENCIES_URL);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);

        $json = json_decode($response, true);

        foreach ($json[0]['rates'] ?? [] as $currency) {
            $this->entityManager->persist(new Currency($currency['code'], $currency['currency']));
        }
        $this->entityManager->flush();
    }
}