<?php

namespace App\Command;

use App\Service\RateImporter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportRates extends Command
{
    protected static $defaultName = 'app:import:rates';
    private $ratesImporter;

    public function __construct(RateImporter $ratesImporter)
    {
        parent::__construct();

        $this->ratesImporter = $ratesImporter;
    }

    protected function configure()
    {
        $this->setDescription('Import rates from NBP api.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->ratesImporter->import();
    }
}