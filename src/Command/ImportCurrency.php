<?php

namespace App\Command;

use App\Service\CurrencyImporter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportCurrency extends Command
{
    protected static $defaultName = 'app:import:currency';
    private $currencyImporter;

    public function __construct(CurrencyImporter $currencyImporter)
    {
        parent::__construct();

        $this->currencyImporter = $currencyImporter;
    }

    protected function configure()
    {
        $this->setDescription('Import currency from NBP api.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->currencyImporter->import();
    }
}