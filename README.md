## On service startup run:
```bash
bin/console app:import:currency
```
## Add to crone on every working day at 12:16 GMT +1
```bash
bin/console app:import:rate
```
##Endpoints
/currencies - return currencies (code, name)

/rate/{code} - return rate

/average-rate/{code} - return average rate